# DevOps/Systems Exercise

[DevOps Frontend Example App](https://gitlab.com/campusedu/public/devops-exercise-frontend)

[DevOps Backend Example App](https://gitlab.com/campusedu/public/devops-exercise-backend)


## Objective
Create a way to deploy the multi-tier service containing a loadbalancer, the above frontend project, the above backend project, and a postgres (v13) database. The objective is evaluate your ability to understand projects at a high level and fit them together with standard services.

## Deliverable
Please create a Merge Request on this repo providing your solution. The solution can be in any form you are most comfortable. This can be any combination of documentation, script file(s) (shell/powershell), programming (Python, Ansible, Terraform, etc.) and/or other infrastructure tools (Virtualbox, Vagrant, Docker, Kubernetes, etc). The goal is create a documented, repeatable pattern for deploying the described scenario that can be committed to code.

Feel free to ask questions for clarification or even open issues on this and the related projects if you find an issue or feels something needs more clarity.












